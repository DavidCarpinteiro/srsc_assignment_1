package Exceptions;

public class RepeatedMessage extends Exception {

    public RepeatedMessage(String msg) {
        super(msg);
    }
}
