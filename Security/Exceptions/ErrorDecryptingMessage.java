package Exceptions;

public class ErrorDecryptingMessage extends Exception {

    public ErrorDecryptingMessage() {
        super();
    }

    public ErrorDecryptingMessage(String msg) {
        super(msg);
    }
}
