package Exceptions;

public class MessageIsTooOld extends Exception {

    public MessageIsTooOld() {
        super();
    }

    public MessageIsTooOld(String msg) {
        super(msg);
    }
}
