package Exceptions;

public class PayloadSizeDoesNotMatch extends Exception {

    public PayloadSizeDoesNotMatch() {
        super();
    }

    public PayloadSizeDoesNotMatch(String msg) {
        super(msg);
    }
}
