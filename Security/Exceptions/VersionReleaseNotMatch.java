package Exceptions;

public class VersionReleaseNotMatch extends Exception {

    public VersionReleaseNotMatch() {
        super();
    }

    public VersionReleaseNotMatch(String msg) {
        super(msg);
    }
}
