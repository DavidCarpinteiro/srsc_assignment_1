package Exceptions;

public class WrongFile extends Exception {

    public WrongFile(String msg) {
        super(msg);
    }
}
