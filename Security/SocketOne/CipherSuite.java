package SocketOne;

import Utils.Tools;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.security.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CipherSuite {
    private static String[] ivAlgs = {"CFB", "OFB", "CBC", "PCBC"};
    private static String[] counterAlgs = {"CTR"};
    private static String[] freeAlgs = {"ECB"};

    private static Cipher cipher_enc;
    private static Cipher cipher_dec;
    private static IvParameterSpec iv_spec = null;
    private static IvParameterSpec nonce_counter = null;

    private static Mac h_mac = null;
    private static MessageDigest hash_digest = null;

    private static Key mac_key;
    private static Key mac_dos_key;
    private static Key session_key;

    private static Map<Tools.PROP, Key> keys = new HashMap<>(3);

    static {
        loadKeys();
        load();
    }

    public static int getSizeOfMAC() {
        if (h_mac != null)
            return h_mac.getMacLength();
        else
            return hash_digest.getDigestLength();
    }

    public static byte[] encrypt(byte[] data, int length) {
        return getBytes(data, length, cipher_enc);
    }

    public static byte[] decrypt(byte[] data, int length) {
        return getBytes(data, length, cipher_dec);
    }

    private static byte[] getBytes(byte[] data, int length, Cipher cipher) {
        byte[] out;

        try {
            out = cipher.doFinal(data, 0, length);

            return out;
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static byte[] sign(byte[] data, int length, MAC_TYPE type) {

        if (type.equals(MAC_TYPE.simple)) {
            return sign(data, length, mac_key);
        } else {
            return sign(data, length, mac_dos_key);
        }

    }

    public static boolean check_sigh(byte[] signature, byte[] data, int data_length, MAC_TYPE type) {

        if (type.equals(MAC_TYPE.simple)) {
            return check_sign(signature, data, data_length, mac_key);
        } else {
            return check_sign(signature, data, data_length, mac_dos_key);
        }

    }

    private static byte[] sign(byte[] data, int length, Key key) {
        byte[] out = null;

        if (h_mac != null) {
            try {
                h_mac.init(key);
                h_mac.update(data, 0, length);
                out = h_mac.doFinal();
                h_mac.reset();
                return out;
            } catch (InvalidKeyException e) {
                System.err.println("Error signing");
                System.exit(-1);
            }
        } else {
            hash_digest.update(data, 0, length);
            out = hash_digest.digest();
            hash_digest.reset();
            return out;
        }

        return out;
    }

    private static boolean check_sign(byte[] signature, byte[] data, int data_length, Key key) {
        byte[] out = sign(data, data_length, key);
        return Arrays.equals(signature, out);
    }

    private static void load() {
        initializeCipher();

        if (!Tools.getProperty(Tools.PROP.mac_ciphersuite).isEmpty())
            initializeMAC();
        else
            initializeHashDigest();
    }

    private static void initializeCipher() {
        String session_algorithm = Tools.getProperty(Tools.PROP.session_ciphersuite);

        try {
            cipher_enc = Cipher.getInstance(session_algorithm);
            cipher_dec = Cipher.getInstance(session_algorithm);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException e) {
            System.err.println("Invalid Cipher Algorithm");
            e.printStackTrace();
            System.exit(-1);
        }

        session_key = keys.get(Tools.PROP.session_key);

        for (String mode : ivAlgs) {
            if (session_algorithm.contains(mode)) {
                byte[] ivBytes = Tools.hexStringToByteArray(Tools.getProperty(Tools.PROP.iv));
                iv_spec = new IvParameterSpec(ivBytes);
                break;
            }
        }
        for (String mode : counterAlgs) {
            if (session_algorithm.contains(mode)) {
                byte[] ivBytes = Tools.hexStringToByteArray(Tools.getProperty(Tools.PROP.iv));
                nonce_counter = new IvParameterSpec(ivBytes);
                break;
            }
        }

        try {
            if (iv_spec != null) {
                cipher_enc.init(Cipher.ENCRYPT_MODE, session_key, iv_spec);
                cipher_dec.init(Cipher.DECRYPT_MODE, session_key, iv_spec);
            } else if (nonce_counter != null) {
                cipher_enc.init(Cipher.ENCRYPT_MODE, session_key, nonce_counter);
                cipher_dec.init(Cipher.DECRYPT_MODE, session_key, nonce_counter);
            } else {
                cipher_enc.init(Cipher.ENCRYPT_MODE, session_key);
                cipher_dec.init(Cipher.DECRYPT_MODE, session_key);
            }
        } catch (InvalidAlgorithmParameterException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    private static void initializeMAC() {
        String h_mac_algo = Tools.getProperty(Tools.PROP.mac_ciphersuite);

        try {
            h_mac = Mac.getInstance(h_mac_algo);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Invalid MAC Algorithm");
            e.printStackTrace();
            System.exit(-1);
        }

        Key key_m = keys.get(Tools.PROP.mac_key);
        Key key_d = keys.get(Tools.PROP.mac_key);

        mac_key = new SecretKeySpec(key_m.getEncoded(), h_mac_algo);
        mac_dos_key = new SecretKeySpec(key_d.getEncoded(), h_mac_algo);
    }

    private static void initializeHashDigest() {
        String hash_algo = Tools.getProperty(Tools.PROP.hash_ciphersuite);

        try {
            hash_digest = MessageDigest.getInstance(hash_algo);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Invalid hash Algorithm");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private static void loadKeys() {
        String keystore = Tools.getProperty(Tools.PROP.keystore_jecks);
        String keystore_password = Tools.getProperty(Tools.PROP.keystore_password);

        String session_key = Tools.getProperty(Tools.PROP.session_key);
        String mac_key = Tools.getProperty(Tools.PROP.mac_key);
        String mac_key_dos = Tools.getProperty(Tools.PROP.mac_key_dos);

        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            FileInputStream stream = new FileInputStream(keystore);
            keyStore.load(stream, keystore_password.toCharArray());

            Key key_s = keyStore.getKey(session_key, keystore_password.toCharArray());
            Key key_m = keyStore.getKey(mac_key, keystore_password.toCharArray());
            Key key_md = keyStore.getKey(mac_key_dos, keystore_password.toCharArray());

            keys.put(Tools.PROP.session_key, key_s);
            keys.put(Tools.PROP.mac_key, key_m);
            keys.put(Tools.PROP.mac_key_dos, key_md);

        } catch (Exception e) {
            System.err.println("Error loading keys");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public enum MAC_TYPE {
        simple, dos
    }

}
