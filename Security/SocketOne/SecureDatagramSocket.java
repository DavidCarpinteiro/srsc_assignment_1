package SocketOne;

import Exceptions.*;

import java.io.IOException;
import java.net.*;

public class SecureDatagramSocket extends DatagramSocket implements java.io.Closeable {

    public SecureDatagramSocket() throws SocketException {

    }

    public SecureDatagramSocket(SocketAddress bindaddr) throws SocketException {
        super(bindaddr);
    }

    public SecureDatagramSocket(int port, InetAddress laddr) throws SocketException {
        super(port, laddr);
    }

    public synchronized void receive(DatagramPacket p) throws IOException {
        super.receive(p);

        try {
            byte[] data = Protocol.getData(p.getData(), p.getLength());

            p.setData(data);
        } catch (VersionReleaseNotMatch | PayloadSizeDoesNotMatch | ErrorDecryptingMessage | RepeatedMessage | WrongFile | MessageIsTooOld e) {
            e.printStackTrace();
            p.setData(new byte[0]);
        }

    }

    public void send(DatagramPacket p) throws IOException {
        byte[] data = Protocol.createMessage(p.getData(), p.getLength(), Protocol.PAYLOAD_TYPE.TYPE_1);
        p.setData(data);

        super.send(p);
    }
}
