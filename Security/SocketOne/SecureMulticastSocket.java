package SocketOne;

import Exceptions.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;

public class SecureMulticastSocket extends MulticastSocket {
    public SecureMulticastSocket() throws IOException {
    }

    public SecureMulticastSocket(int port) throws IOException {
        super(port);
    }

    public SecureMulticastSocket(SocketAddress bindaddr) throws IOException {
        super(bindaddr);
    }

    public synchronized void receive(DatagramPacket p) throws IOException {
        super.receive(p);

        try {
            byte[] data = Protocol.getData(p.getData(), p.getLength());

            p.setData(data);
        } catch (VersionReleaseNotMatch | PayloadSizeDoesNotMatch | ErrorDecryptingMessage | RepeatedMessage | WrongFile | MessageIsTooOld e) {
            e.printStackTrace();
            p.setData(new byte[0]);
        }

    }

    public void send(DatagramPacket p) throws IOException {
        byte[] data = Protocol.createMessage(p.getData(), p.getLength(), Protocol.PAYLOAD_TYPE.TYPE_1);
        p.setData(data);

        super.send(p);
    }

    public void joinGroup(InetAddress mcastaddr) throws IOException {
        super.joinGroup(mcastaddr);
    }
}
