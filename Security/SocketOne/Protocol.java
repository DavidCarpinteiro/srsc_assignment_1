package SocketOne;

import Exceptions.*;
import Utils.Tools;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Protocol {
    private static final int HEADER_SIZE = 15;
    private static final byte SEPARATOR = 0x00;
    private static final int VERSION_SIZE = 1;
    private static final int PAYLOAD_TYPE_SIZE = 1;
    private static final int PAYLOAD_SIZE_SIZE = 2;
    private static final int SEPARATOR_SIZE = 1;
    private static final int ID_SIZE = 2;
    private static final int NONCE_SIZE = 8;
    private static final int TIMES_TAMP_SIZE = 8;
    private static byte VERSION_RELEASE = 0b00010001;
    private static final int LIMIT = 5 * 1000;
    private static final SecureRandom sr = new SecureRandom();

    private static short sequence_id = -1;
    private static final Set<Long> prevNonces = new HashSet<>(10000);
    private static final Set<Long> prevNoncesRec = new HashSet<>(10000);

    private static long getNextNonce() {
        long l;
        do {
            l = sr.nextLong();
        } while (!prevNonces.add(l));
        return l;
    }

    private static boolean isGoodNonce(long nonce) {
        if(prevNoncesRec.contains(nonce))
            return false;
        prevNoncesRec.add(nonce);
        return true;
    }

    public static short getId() {
        return sequence_id;
    }

    public static void setId(short id) {
        Protocol.sequence_id = id;
    }

    private static byte[] createHeader(PAYLOAD_TYPE type, int size) {
        final byte payload_type = type.type;

        final byte[] payload_size = new byte[2];
        payload_size[0] = (byte) ((short) size & 0xff);
        payload_size[1] = (byte) (((short) size >> 8) & 0xff);
        final byte[] time_stamp = Tools.longToBytes(System.currentTimeMillis());

        final byte[] tmp  = new byte[]{VERSION_RELEASE, SEPARATOR, payload_type, SEPARATOR, payload_size[0], payload_size[1], SEPARATOR};
        return concat(tmp, tmp.length, time_stamp, time_stamp.length);
    }

    private static byte[] createPayload(byte[] data, int size) {
        final byte[] id_bytes = new byte[ID_SIZE];
        id_bytes[0] = (byte) (sequence_id & 0xff);
        id_bytes[1] = (byte) ((sequence_id >> 8) & 0xff);

        final byte[] nonce_bytes = Tools.longToBytes(getNextNonce());

        final byte[] id_nonce = concat(id_bytes, id_bytes.length, nonce_bytes, nonce_bytes.length);
        final byte[] full_data = concat(id_nonce, id_nonce.length, data, size);

        final byte[] mac_data = CipherSuite.sign(full_data, full_data.length, CipherSuite.MAC_TYPE.simple);

        final byte[] msg = concat(full_data, full_data.length, mac_data, mac_data.length);

        final byte[] enc_data = CipherSuite.encrypt(msg, msg.length);
        final byte[] mac_enc = CipherSuite.sign(enc_data, enc_data.length, CipherSuite.MAC_TYPE.dos);

        return concat(enc_data, enc_data.length, mac_enc, mac_enc.length);
    }

    public static byte[] createMessage(byte[] data, int size, PAYLOAD_TYPE type) {
        final byte[] payload = createPayload(data, size);
        final byte[] header = createHeader(type, payload.length);
        return concat(header, header.length, payload, payload.length);
    }

    private static byte[] extractHeader(byte[] data) throws VersionReleaseNotMatch, MessageIsTooOld {
        int current_length = 0;
        final byte version_release = Arrays.copyOf(data, VERSION_SIZE)[0];
        current_length = VERSION_SIZE + SEPARATOR_SIZE;
        final byte[] payload_type = Arrays.copyOfRange(data, current_length, current_length + PAYLOAD_TYPE_SIZE);
        current_length += PAYLOAD_TYPE_SIZE + SEPARATOR_SIZE;
        final byte[] payload_size = Arrays.copyOfRange(data, current_length, current_length + PAYLOAD_SIZE_SIZE);
        current_length += PAYLOAD_SIZE_SIZE + SEPARATOR_SIZE;
        final byte[] time_stamp = Arrays.copyOfRange(data, current_length, current_length + TIMES_TAMP_SIZE);

        final long time_limit = System.currentTimeMillis() - LIMIT;
        if(time_limit > Tools.bytesToLong(time_stamp)) {
            throw new MessageIsTooOld(String.format(" %d >> %d\n", time_limit, Tools.bytesToLong(time_stamp)));
        }

        // Verify version-release
        if (version_release != VERSION_RELEASE) {
            throw new VersionReleaseNotMatch(String.format("Version release does not mach: 0x%02X != 0x%02X\n", version_release, VERSION_RELEASE));
        }

        return concat(payload_type, payload_type.length, payload_size, payload_size.length);
    }

    private static byte[] extractPayload(byte[] data) throws ErrorDecryptingMessage {
        final int size_of_mac = CipherSuite.getSizeOfMAC();
        final int size_of_encryption = data.length - size_of_mac;

        final byte[] encryption = Arrays.copyOf(data, size_of_encryption);
        final byte[] signature = Arrays.copyOfRange(data, size_of_encryption, data.length);

        final boolean signature_ok = CipherSuite.check_sigh(signature, encryption, encryption.length, CipherSuite.MAC_TYPE.dos);

        if (!signature_ok) {
            throw new ErrorDecryptingMessage("Siagnatures do not match");
        }

        final byte[] plain = CipherSuite.decrypt(encryption, encryption.length);
        if (plain == null) {
            throw new ErrorDecryptingMessage("Could not decrypt");
        }

        final int size_of_message = plain.length - size_of_mac;

        final byte[] msg = Arrays.copyOf(plain, size_of_message);
        final byte[] msg_signature = Arrays.copyOfRange(plain, size_of_message, plain.length);

        if (!CipherSuite.check_sigh(msg_signature, msg, msg.length, CipherSuite.MAC_TYPE.simple)) {
            throw new ErrorDecryptingMessage("Message signature does not match");
        }

        return msg;
    }

    public static byte[] getData(byte[] message, int size) throws VersionReleaseNotMatch, PayloadSizeDoesNotMatch, ErrorDecryptingMessage, RepeatedMessage, WrongFile, MessageIsTooOld {
        final byte[] header = Arrays.copyOf(message, HEADER_SIZE);
        final byte[] payload = Arrays.copyOfRange(message, HEADER_SIZE, size);

        final byte[] extracted_header = extractHeader(header);

        final byte payload_type = Arrays.copyOf(extracted_header, PAYLOAD_TYPE_SIZE)[0];
        final byte[] payload_size = Arrays.copyOfRange(extracted_header, PAYLOAD_TYPE_SIZE, PAYLOAD_TYPE_SIZE + PAYLOAD_SIZE_SIZE);

        // Check Payload Size
        final short actual_size = (short) (((payload_size[1] & 0xFF) << 8) | (payload_size[0] & 0xFF));
        if ((short) payload.length != actual_size) {
            throw new PayloadSizeDoesNotMatch(String.format("Payload size does not match: %d != %d", (short) payload.length, actual_size));
        }

        // Select OP based on type
        final PAYLOAD_TYPE type = PAYLOAD_TYPE.getType(payload_type);

        final byte[] extracted_payload = extractPayload(payload);

        final byte[] payload_id = Arrays.copyOfRange(extracted_payload, 0, ID_SIZE);
        final short curr_id = (short) (((payload_id[1] & 0xFF) << 8) | (payload_id[0] & 0xFF));
        if (sequence_id == -1)
            sequence_id = curr_id;
        else if (sequence_id != curr_id)
            throw new WrongFile("Expected " + sequence_id + " but got " + curr_id);

        final byte[] payload_nonce = Arrays.copyOfRange(extracted_payload, ID_SIZE, ID_SIZE + NONCE_SIZE);
        final long curr_nonce = Tools.bytesToLong(payload_nonce);

        if (!isGoodNonce(curr_nonce))
            throw new RepeatedMessage("Repeated Message Detected: " + curr_nonce);


        final byte[] msg = Arrays.copyOfRange(extracted_payload, ID_SIZE + NONCE_SIZE, extracted_payload.length);

        return msg;
    }

    private static byte[] concat(byte[] a, int a_l, byte[] b, int b_l) {
        final byte[] c = new byte[a_l + b_l];
        System.arraycopy(a, 0, c, 0, a_l);
        System.arraycopy(b, 0, c, a_l, b_l);
        return c;
    }

    enum PAYLOAD_TYPE {
        TYPE_1((byte) 0x01), TYPE_2((byte) 0x02);

        private byte type;

        PAYLOAD_TYPE(byte type) {
            this.type = type;
        }

        public static PAYLOAD_TYPE getType(byte b) {
            for (PAYLOAD_TYPE t : values()) {
                if (t.type == b) {
                    return t;
                }
            }
            return null;
        }

        public byte getType() {
            return this.type;
        }
    }
}
