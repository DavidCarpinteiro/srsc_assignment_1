package Utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class MovieDB {

    private static Map<String, Short> movies;

    public static void init(String path) {
        movies = new HashMap<>();

        short ids = 0;
        final File folder = new File(path);
        System.out.println(path);
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                // do nothing
            } else {
                movies.put(fileEntry.getName(), ++ids);
            }
        }
    }

    public static Short getId(String title) {
        return movies.get(title);
    }
}
