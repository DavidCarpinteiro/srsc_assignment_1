package Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Tools {
    private static final String CONFIG_HOME = "Security/Config/ciphersuite.properties";
    private static final Map<PROP, String> configs = new HashMap<>();
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    static {
        loadConfig();
    }

    public static String getProperty(PROP prop) {
        return configs.get(prop);
    }

    private static void loadConfig() {
        try (InputStream inputStream = new FileInputStream(CONFIG_HOME)) {
            Properties properties = new Properties();
            properties.load(inputStream);

            for (PROP p : PROP.values()) {
                String v = properties.getProperty(p.toString());
                configs.put(p, v);
            }

            for(Map.Entry<PROP, String> e : configs.entrySet()){
                if(e.getValue().isEmpty()) {
                    if(e.getValue().equals(PROP.hash_ciphersuite.toString()))
                        if(!configs.get(PROP.mac_ciphersuite).isEmpty())
                            continue;
                    if(e.getValue().equals(PROP.mac_ciphersuite.toString()))
                        if(!configs.get(PROP.hash_ciphersuite).isEmpty())
                            continue;
                    throw new RuntimeException("Cipher config file is incomplete");
                }
            }
        } catch (IOException e) {
            System.err.println("Error loading config file");
            System.exit(-1);
        }
    }

    public static String bytesToHex(byte[] bytes, int length) {
        char[] hexChars = new char[length * 2];
        for (int j = 0; j < length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(0, x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(bytes, 0, bytes.length);
        buffer.flip();
        return buffer.getLong();
    }

    public enum PROP {
        session_ciphersuite, hash_ciphersuite, mac_ciphersuite, keystore_jecks, keystore_password, session_key, mac_key, mac_key_dos, iv
    }
}
