/* hjUDPproxy, 20/Mar/18
 *
 * This is a very simple (transparent) UDP proxy
 * The proxy can listening on a remote source (server) UDP sender
 * and transparently forward received datagram packets in the
 * delivering endpoint
 *
 * Possible Remote listening endpoints:
 *    Unicast IP address and port: configurable in the file config.properties
 *    Multicast IP address and port: configurable in the code
 *
 * Possible local listening endpoints:
 *    Unicast IP address and port
 *    Multicast IP address and port
 *       Both configurable in the file config.properties
 */

import SocketOne.SecureDatagramSocket;
import SocketOne.SecureMulticastSocket;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.*;
import java.security.Security;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

class hjUDPproxy {
    public static void main(String[] args) throws Exception {
        Security.insertProviderAt(new BouncyCastleProvider(), 0);

        Properties properties = new Properties();
        try (InputStream inputStream = new FileInputStream("hjUDPproxy/config.properties");) {
            properties.load(inputStream);
        } catch (FileNotFoundException e) {
            System.err.println("Configuration file not found!");
            System.exit(1);
        }

        String remote = properties.getProperty("remote");
        String destinations = properties.getProperty("localdelivery");

        InetSocketAddress inSocketAddress = parseSocketAddress(remote);
        Set<SocketAddress> outSocketAddressSet = Arrays.stream(destinations.split(",")).map(s -> parseSocketAddress(s)).collect(Collectors.toSet());

        // Choose between unicast and multicast
        DatagramSocket inSocket;
        if (inSocketAddress.getAddress().isMulticastAddress()) {
            inSocket = new SecureMulticastSocket(inSocketAddress.getPort());
            ((SecureMulticastSocket) inSocket).joinGroup(InetAddress.getByName(inSocketAddress.getAddress().getHostAddress()));
        } else {
            inSocket = new SecureDatagramSocket(inSocketAddress);
        }

        DatagramSocket outSocket = new DatagramSocket();
        byte[] buffer = new byte[4 * 1024];
        while (true) {
            DatagramPacket inPacket = new DatagramPacket(buffer, buffer.length);

            inSocket.receive(inPacket);

            System.out.print(":");// debug
            for (SocketAddress outSocketAddress : outSocketAddressSet) {
                outSocket.send(new DatagramPacket(inPacket.getData(), inPacket.getLength(), outSocketAddress));
            }
        }
    }

    private static InetSocketAddress parseSocketAddress(String socketAddress) {
        String[] split = socketAddress.split(":");
        String host = split[0];
        int port = Integer.parseInt(split[1]);
        return new InetSocketAddress(host, port);
    }
}
